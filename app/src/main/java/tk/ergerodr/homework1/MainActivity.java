package tk.ergerodr.homework1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button b00;
    private Button b01;
    private Button b02;
    private Button b10;
    private Button b11;
    private Button b12;
    private Button b20;
    private Button b21;
    private Button b22;
    private Button restart_button;

    private static final int ROWS = 3, COLS = 3, EMPTY = 0, CIRCLE = 1, CROSS = 2;
    private static final String KEY_STATE00 = "State00";
    private static final String KEY_STATE01 = "State01";
    private static final String KEY_STATE02 = "State02";
    private static final String KEY_STATE10 = "State10";
    private static final String KEY_STATE11 = "State11";
    private static final String KEY_STATE12 = "State12";
    private static final String KEY_STATE20 = "State20";
    private static final String KEY_STATE21 = "State21";
    private static final String KEY_STATE22 = "State22";
    private static final String KEY_PLAYER = "pState";
    private static final String KEY_CLICK00 = "Click00";
    private static final String KEY_CLICK01 = "Click01";
    private static final String KEY_CLICK02 = "Click02";
    private static final String KEY_CLICK10 = "Click10";
    private static final String KEY_CLICK11 = "Click11";
    private static final String KEY_CLICK12 = "Click12";
    private static final String KEY_CLICK20 = "Click20";
    private static final String KEY_CLICK21 = "Click21";
    private static final String KEY_CLICK22 = "Click22";

    private static String tmpStr = "";

    private static int[][] board = new int[ROWS][COLS];
    private boolean player1 = true;
    private int toastResId = 0;

    protected void whoseTurn(Boolean player){
        if(player1) {
            toastResId = R.string.player1_toast;
            Toast.makeText(this, toastResId, Toast.LENGTH_SHORT).show();
        } else {
            toastResId = R.string.player2_toast;
            Toast.makeText(this, toastResId, Toast.LENGTH_SHORT).show();
        }
    }

    protected void resetGame() {
        b00.setText("");b01.setText("");b02.setText("");
        b10.setText("");b11.setText("");b12.setText("");
        b20.setText("");b21.setText("");b22.setText("");
        b00.setClickable(true);b01.setClickable(true);b02.setClickable(true);
        b10.setClickable(true);b11.setClickable(true);b12.setClickable(true);
        b20.setClickable(true);b21.setClickable(true);b22.setClickable(true);
        player1 = true;
    }

    protected void disableAllButtons() {
        b00.setClickable(false);b01.setClickable(false);b02.setClickable(false);
        b10.setClickable(false);b11.setClickable(false);b12.setClickable(false);
        b20.setClickable(false);b21.setClickable(false);b22.setClickable(false);
    }

    protected void resetBoard() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                board[row][col] = EMPTY;
            }
        }
    }

    protected Boolean hasWon(int symbol, int currentRow, int currentCol){
        return ((board[currentRow][0] == symbol
                && board[currentRow][1] == symbol //check row
                && board[currentRow][2] == symbol)

            || (board[0][currentCol] == symbol
                && board[1][currentCol] == symbol //check column
                && board[2][currentCol] == symbol)

            || (board[0][0] == symbol
                && board[1][1] == symbol //check diagonal
                && board[2][2] == symbol)

            || (board[0][2] == symbol
                && board[1][1] == symbol//check other diagonal
                && board[2][0] == symbol));
    }

    protected void saveStates(Bundle Instance){//save all info into Bundle Object
        //save player state
        Instance.putBoolean(KEY_PLAYER, player1);
        //save button text states
        Instance.putString(KEY_STATE00, b00.getText().toString());
        Instance.putString(KEY_STATE01, b01.getText().toString());
        Instance.putString(KEY_STATE02, b02.getText().toString());
        Instance.putString(KEY_STATE10, b10.getText().toString());
        Instance.putString(KEY_STATE11, b11.getText().toString());
        Instance.putString(KEY_STATE12, b12.getText().toString());
        Instance.putString(KEY_STATE20, b20.getText().toString());
        Instance.putString(KEY_STATE21, b21.getText().toString());
        Instance.putString(KEY_STATE22, b22.getText().toString());
        //save button clickable states.
        Instance.putBoolean(KEY_CLICK00, b00.isClickable());
        Instance.putBoolean(KEY_CLICK01, b01.isClickable());
        Instance.putBoolean(KEY_CLICK02, b02.isClickable());
        Instance.putBoolean(KEY_CLICK10, b10.isClickable());
        Instance.putBoolean(KEY_CLICK11, b11.isClickable());
        Instance.putBoolean(KEY_CLICK12, b12.isClickable());
        Instance.putBoolean(KEY_CLICK20, b20.isClickable());
        Instance.putBoolean(KEY_CLICK21, b21.isClickable());
        Instance.putBoolean(KEY_CLICK22, b22.isClickable());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b00 = (Button) findViewById(R.id.Button00);
        b00.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1){
                    board [0][0] = CIRCLE;
                    b00.setText(R.string.letter_o);
                    b00.setClickable(false);
                    if(hasWon(CIRCLE, 0, 0)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    }else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else{
                    board [0][0] = CROSS;
                    b00.setText(R.string.letter_x);
                    b00.setClickable(false);
                    if(hasWon(CROSS, 0, 0)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b01 = (Button) findViewById(R.id.Button01);
        b01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [0][1] = CIRCLE;
                    b01.setText(R.string.letter_o);
                    b01.setClickable(false);
                    if(hasWon(CIRCLE, 0, 1)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [0][1] = CROSS;
                    b01.setText(R.string.letter_x);
                    b01.setClickable(false);
                    if(hasWon(CROSS, 0, 1)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b02 = (Button) findViewById(R.id.Button02);
        b02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [0][2] = CIRCLE;
                    b02.setText(R.string.letter_o);
                    b02.setClickable(false);
                    if(hasWon(CIRCLE, 0, 2)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [0][2] = CROSS;
                    b02.setText(R.string.letter_x);
                    b02.setClickable(false);
                    if(hasWon(CROSS, 0, 2)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b10 = (Button) findViewById(R.id.Button10);
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [1][0] = CIRCLE;
                    b10.setText(R.string.letter_o);
                    b10.setClickable(false);
                    if(hasWon(CIRCLE, 1, 0)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [1][0] = CROSS;
                    b10.setText(R.string.letter_x);
                    b10.setClickable(false);
                    if(hasWon(CROSS, 1, 0)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b11 = (Button) findViewById(R.id.Button11);
        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [1][1] = CIRCLE;
                    b11.setText(R.string.letter_o);
                    b11.setClickable(false);
                    if(hasWon(CIRCLE, 1, 1)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [1][1] = CROSS;
                    b11.setText(R.string.letter_x);
                    b11.setClickable(false);
                    if(hasWon(CROSS, 1, 1)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b12 = (Button) findViewById(R.id.Button12);
        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [1][2] = CIRCLE;
                    b12.setText(R.string.letter_o);
                    b12.setClickable(false);
                    if(hasWon(CIRCLE, 1, 2)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [1][2] = CROSS;
                    b12.setText(R.string.letter_x);
                    b12.setClickable(false);
                    if(hasWon(CROSS, 1, 2)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b20 = (Button) findViewById(R.id.Button20);
        b20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [2][0] = CIRCLE;
                    b20.setText(R.string.letter_o);
                    b20.setClickable(false);
                    if(hasWon(CIRCLE, 2, 0)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [2][0] = CROSS;
                    b20.setText(R.string.letter_x);
                    b20.setClickable(false);
                    if(hasWon(CROSS, 2, 0)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b21 = (Button) findViewById(R.id.Button21);
        b21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1) {
                    board [2][1] = CIRCLE;
                    b21.setText(R.string.letter_o);
                    b21.setClickable(false);
                    if(hasWon(CIRCLE, 2, 1)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [2][1] = CROSS;
                    b21.setText(R.string.letter_x);
                    b21.setClickable(false);
                    if(hasWon(CROSS, 2, 1)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        b22 = (Button) findViewById(R.id.Button22);
        b22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player1){
                    board [2][2] = CIRCLE;
                    b22.setText(R.string.letter_o);
                    b22.setClickable(false);
                    if(hasWon(CIRCLE, 2, 2)){
                        toastResId = R.string.player1_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = false;
                        whoseTurn(player1);
                    }
                } else {
                    board [2][2] = CROSS;
                    b22.setText(R.string.letter_x);
                    b22.setClickable(false);
                    if(hasWon(CROSS, 2, 2)){
                        toastResId = R.string.player2_wins;
                        Toast.makeText(MainActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                        disableAllButtons();
                    } else {
                        player1 = true;
                        whoseTurn(player1);
                    }
                }
            }
        });

        restart_button = (Button) findViewById(R.id.restart_button);
        restart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                resetGame();
                resetBoard();
            }
        });

        if(savedInstanceState != null){ //load all info from saveStates
            //Load text from saved Bundle
            player1 = savedInstanceState.getBoolean(KEY_PLAYER);
            b00.setText(savedInstanceState.getString(KEY_STATE00));
            b01.setText(savedInstanceState.getString(KEY_STATE01));
            b02.setText(savedInstanceState.getString(KEY_STATE02));
            b10.setText(savedInstanceState.getString(KEY_STATE10));
            b11.setText(savedInstanceState.getString(KEY_STATE11));
            b12.setText(savedInstanceState.getString(KEY_STATE12));
            b20.setText(savedInstanceState.getString(KEY_STATE20));
            b21.setText(savedInstanceState.getString(KEY_STATE21));
            b22.setText(savedInstanceState.getString(KEY_STATE22));
            //Load if a button is clickable from saved Bundle
            b00.setClickable(savedInstanceState.getBoolean(KEY_CLICK00));
            b01.setClickable(savedInstanceState.getBoolean(KEY_CLICK01));
            b02.setClickable(savedInstanceState.getBoolean(KEY_CLICK02));
            b10.setClickable(savedInstanceState.getBoolean(KEY_CLICK10));
            b11.setClickable(savedInstanceState.getBoolean(KEY_CLICK11));
            b12.setClickable(savedInstanceState.getBoolean(KEY_CLICK12));
            b20.setClickable(savedInstanceState.getBoolean(KEY_CLICK20));
            b21.setClickable(savedInstanceState.getBoolean(KEY_CLICK21));
            b22.setClickable(savedInstanceState.getBoolean(KEY_CLICK22));
        }

    }

    @Override//save any necessary information into a Bundle object
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        saveStates(savedInstanceState);
    }

}
